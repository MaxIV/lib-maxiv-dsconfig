import json
try:
    from unittest.mock import MagicMock
except ImportError:
    from mock import MagicMock

import tango

from dsconfig.json2tango import apply_config, show_output


original_config = {
    "servers": {
        "TangoTest": {
            "test": {
                "TangoTest": {
                    "sys/tg_test/1": {},
                    "sys/tg_test/2": {}
                }
            }
        }
    }
}

config = {
    "servers": {
        "TangoTest": {
            "test": {
                "TangoTest": {
                    "sys/tg_test/1": {},
                    "sys/tg_test/2": {
                        "alias": "hello"
                    },
                }
            }
        }
    }
}

config2 = {
    "servers": {
        "TangoTest": {
            "test": {
                "TangoTest": {
                    "sys/tg_test/1": {},
                    "sys/tg_test/2": {},
                    "sys/tg_test/3": {
                        "alias": "hello"
                    },
                }
            }
        }
    }
}

# Move all devices to a new server
config3 = {
    "servers": {
        "TangoTest": {
            "test2": {
                "TangoTest": {
                    "sys/tg_test/1": {},
                    "sys/tg_test/2": {}
                }
            }
        }
    }
}


def test_apply_config__ignores_noncolliding_alias():
    db = MagicMock(spec=tango.Database)
    db.command_inout.return_value = [], ["alias2", "some/other/device"]
    _, _, dbcalls, _, alias_collisions, _ = apply_config(config, db, original=original_config)
    assert dbcalls == [
        ("put_device_alias", ("sys/tg_test/2", "hello",), {})
    ]


def test_apply_config__removes_colliding_alias():
    db = MagicMock(spec=tango.Database)
    db.command_inout.return_value = [], ["hello", "sys/tg_test/1",
                                         "alias2", "some/other/device"]
    _, _, dbcalls, _, alias_collisions, _ = apply_config(config, db, original=original_config)
    assert dbcalls == [
        ("delete_device_alias", ("hello",), {}),
        ("put_device_alias", ("sys/tg_test/2", "hello",), {})
    ]


def test_apply_config__removes_colliding_alias_new_device():
    db = MagicMock(spec=tango.Database)
    db.command_inout.return_value = [], ["hello", "some/other/device"]
    _, _, dbcalls, _, alias_collisions, _ = apply_config(config2, db, original=original_config)
    assert len(dbcalls) == 3
    assert dbcalls[0] == ('delete_device_alias', ('hello',), {})
    assert dbcalls[1][0] == "add_device"
    assert dbcalls[2] == ("put_device_alias", ("sys/tg_test/3", "hello",), {})


def test_apply_config__remove_alias():
    db = MagicMock(spec=tango.Database)
    db.command_inout.return_value = [], ["hello", "sys/tg_test/1"]
    _, _, dbcalls, _, alias_collisions, _ = apply_config(original_config, db, original=original_config)
    assert dbcalls == [('delete_device_alias', ('hello',), {})]


def test_apply_config__remove_empty_server():
    db = MagicMock(spec=tango.Database)
    db.command_inout.return_value = [], []
    db.get_device_class_list.return_value = []
    _, _, dbcalls, _, _, _ = apply_config(config3, db, original=original_config)
    assert len(dbcalls) == 3
    assert dbcalls[0][0] == "add_device"
    assert dbcalls[1][0] == "add_device"
    assert dbcalls[2] == ("delete_server", ("TangoTest/test",), {})


def test_show_output__json_format(capsys):
    """
    Check that the JSON output format is correct.
    It should not be changed without care, as there might be
    external programs that rely on it.
    """
    old_config = {
        "servers": {
            "TangoTest": {
                "test": {
                    "TangoTest": {
                        "sys/tg_test/1": {
                            "properties": {
                                "bananas": ["123"]
                            }
                        },
                        "sys/tg_test/2": {}
                    }
                }
            }
        }
    }
    new_config = {
        "servers": {
            "TangoTest": {
                "test": {
                    "TangoTest": {
                        "sys/tg_test/1": {
                            "properties": {
                                "something": ["abc"]
                            }
                        }
                    }
                }
            },
            "MangoTest": {
                "1": {
                    "MangoTest": {
                        "sys/mango/1": {}
                    }
                }
            }
        }
    }
    db = MagicMock(spec=tango.Database)
    db.command_inout.return_value = [], []
    results = apply_config(new_config, db, original=old_config)
    show_output(*results, show_json=True)
    captured = capsys.readouterr()
    output = json.loads(captured.out)
    expected = {
        "devices": {
            # Deleted device
            "sys/tg_test/2": {
                "deleted": True,
                "server": "TangoTest/test",
                "instance": "test",
                "device_class": "TangoTest",
                "properties": {}
            },
            "sys/tg_test/1": {
                "properties": {
                    # Removed property
                    "bananas": {
                        "old_value": [
                            "123"
                        ]
                    },
                    # Added property
                    "something": {
                        "value": [
                            "abc"
                        ],
                        "old_value": None
                    }
                }
            },
            # Added device
            "sys/mango/1": {
                "added": True,
                "server": "MangoTest/1",
                "device_class": "MangoTest"
            }
        },
        "classes": {}
    }
    assert output == expected
